﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace WidzeReaguje
{
    public interface IObservationActivity
    {
        string ObservatorName { get; }
        string Branch { get; }
        string ObservationTime { get; }
        int SelectedObservation { get; }
        int Location { get; set; }
        bool IsObserved { get; set; }
        bool IsDangerous { get; set; }
        string DangerWhile { get; set; }
        string DangerWhat { get; set; }
        string DangerReason { get; set; }
        string DangerSolution { get; set; }
        int Control { get; set; }
    }
}