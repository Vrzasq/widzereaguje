﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace WidzeReaguje
{
    public class ObservationContainer
    {
        private Dictionary<int, ObservationEntity> observations = new Dictionary<int, ObservationEntity>();
        private IObservationActivity activity;

        public ObservationContainer(IObservationActivity activity)
        {
            this.activity = activity;
        }

        public void SaveActivityState(int observationIndex)
        {
            ObservationEntity entity = GetCurrentState();
            if (observations.ContainsKey(observationIndex))
                observations[observationIndex] = entity;
            else
                observations.Add(observationIndex, entity);
        }

        public void LoadActivityState(int observationIndex)
        {
            ObservationEntity entity;
            if (!observations.TryGetValue(observationIndex, out entity))
            {
                entity = GetDefaultState();
                observations.Add(observationIndex, entity);
            }

            LoadState(entity);
        }

        private void LoadState(ObservationEntity entity)
        {
            activity.Control = entity.Control;
            activity.DangerReason = entity.DangerWhy;
            activity.DangerSolution = entity.DangerSolution;
            activity.DangerWhat = entity.DangerWhat;
            activity.DangerWhile = entity.DangerWhile;
            activity.IsDangerous = entity.IsDangerous;
            activity.IsObserved = entity.IsObserved;
            activity.Location = entity.Location;
        }

        private ObservationEntity GetDefaultState()
        {
            return new ObservationEntity
            {
                Observator = activity.ObservatorName,
                Branch = activity.Branch,
                ObservationDateTime = activity.ObservationTime
            };
        }

        private ObservationEntity GetCurrentState()
        {
            return new ObservationEntity
            {
                Behavior = activity.SelectedObservation,
                Branch = activity.Branch,
                Control = activity.Control,
                DangerSolution = activity.DangerSolution,
                DangerWhat = activity.DangerWhat,
                DangerWhile = activity.DangerWhile,
                DangerWhy = activity.DangerReason,
                IsDangerous = activity.IsDangerous,
                IsObserved = activity.IsObserved,
                Location = activity.Location,
                ObservationDateTime = activity.ObservationTime,
                Observator = activity.ObservatorName,
            };
        }
    }
}