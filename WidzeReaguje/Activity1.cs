﻿using Android.App;
using Android.Content;
using Android.OS;
using Android.Widget;

namespace WidzeReaguje
{
    [Activity(Label = "Widze Reaguje")]
    public class Activity1 : Activity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.layout1);
            Button startButton = FindViewById<Button>(Resource.Id.button2);

            startButton.Click += (sender, args) =>
            {
                var intent = new Intent(this, typeof(MainActivity));
                StartActivity(intent);
            };
        }
    }
}