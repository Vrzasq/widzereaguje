﻿namespace WidzeReaguje
{
    public class ObservationEntity
    {
        public string Observator { get; set; }
        public string Branch { get; set; }
        public string ObservationDateTime { get; set; }
        public int Behavior { get; set; }
        public bool IsObserved { get; set; }
        public int Location { get; set; }
        public bool IsDangerous { get; set; }
        public string DangerWhile { get; set; } = "";
        public string DangerWhat { get; set; } = "";
        public string DangerWhy { get; set; } = "";
        public string DangerSolution { get; set; } = "";
        public int Control { get; set; }
    }
}