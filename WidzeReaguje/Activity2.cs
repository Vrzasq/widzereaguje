﻿using Android.App;
using Android.Content;
using Android.OS;
using Android.Widget;

namespace WidzeReaguje
{
    [Activity(Label = "Widze Reaguje", MainLauncher = true)]
    public class Activity2 : Activity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            SetContentView(Resource.Layout.layout2);

            SetupLogginButton();
        }

        private void SetupLogginButton()
        {
            Button loginButton = FindViewById<Button>(Resource.Id.sendLoginRequest);
            loginButton.Click += (sender, args) =>
            {
                EditText login = FindViewById<EditText>(Resource.Id.loginView);
                EditText password = FindViewById<EditText>(Resource.Id.passwordView);

                if (login.Text == "jakub.romel" && password.Text == "1234")
                {
                    Intent newActivity = new Intent(this, typeof(Activity1));
                    StartActivity(newActivity);
                }
                else
                    Toast.MakeText(this, "invalid login or password", ToastLength.Long).Show();
            };
        }
    }
}