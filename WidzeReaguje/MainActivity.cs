﻿using System;
using Android.App;
using Android.OS;
using Android.Views;
using Android.Widget;

namespace WidzeReaguje
{
    [Activity(Label = "Widze Reaguje",
        Icon = "@drawable/icon",
        ConfigurationChanges = Android.Content.PM.ConfigChanges.Orientation,
        ScreenOrientation = Android.Content.PM.ScreenOrientation.Portrait)]
    public class MainActivity : Activity, IObservationActivity
    {
        Spinner questionSpinner, locationSpinner, controlSpinner;
        Switch isObservedSwitch, isDangerSwitch;
        TextView observatorName, observationTime, branch;
        EditText dangerWhile, dangerWhat, dangerReason, dangerSolution;

        public string ObservatorName => observatorName.Text;
        public string Branch => branch.Text;
        public string ObservationTime => observationTime.Text;
        public int SelectedObservation => questionSpinner.SelectedItemPosition;

        public int Location { get => locationSpinner.SelectedItemPosition; set => locationSpinner.SetSelection(value); }
        public bool IsObserved { get => isObservedSwitch.Checked; set => isObservedSwitch.Checked = value; }
        public bool IsDangerous { get => isDangerSwitch.Checked; set => isDangerSwitch.Checked = value; }
        public string DangerWhile { get => dangerWhile.Text; set => dangerWhile.Text = value; }
        public string DangerWhat { get => dangerWhat.Text; set => dangerWhat.Text = value; }
        public string DangerReason { get => dangerReason.Text; set => dangerReason.Text = value; }
        public string DangerSolution { get => dangerSolution.Text; set => dangerSolution.Text = value; }
        public int Control { get => controlSpinner.SelectedItemPosition; set => controlSpinner.SetSelection(value); }

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
            SetContentView(Resource.Layout.Main);
            ObservationContainer observationContainer = new ObservationContainer(this);

            FindReferances();
            observationTime.Text = DateTime.Now.ToString();
            SetupQuestionSpinner(observationContainer);
            SetupButtons(observationContainer);

            SetupDangerGridLayout();
        }

        public override void OnBackPressed()
        {
            Toast.MakeText(this, "Press cancel to stop observation", ToastLength.Long).Show();
        }

        private void FindReferances()
        {
            questionSpinner = FindViewById<Spinner>(Resource.Id.spinnerQuestion);
            locationSpinner = FindViewById<Spinner>(Resource.Id.spinnerPlaces);
            controlSpinner = FindViewById<Spinner>(Resource.Id.spinnerControl);
            isObservedSwitch = FindViewById<Switch>(Resource.Id.isObservedSwitch);
            isDangerSwitch = FindViewById<Switch>(Resource.Id.dangerSwitch);
            observatorName = FindViewById<TextView>(Resource.Id.obserwatorName);
            observationTime = FindViewById<TextView>(Resource.Id.observationDate);
            branch = FindViewById<TextView>(Resource.Id.observationDate);
            dangerWhile = FindViewById<EditText>(Resource.Id.dangerText1);
            dangerWhat = FindViewById<EditText>(Resource.Id.dangerText2);
            dangerReason = FindViewById<EditText>(Resource.Id.dangerText3);
            dangerSolution = FindViewById<EditText>(Resource.Id.dangerText4);
        }

        private void SetupQuestionSpinner(ObservationContainer observationContainer)
        {
            questionSpinner.ItemSelected += (sender, e) =>
            {
                observationContainer.LoadActivityState(SelectedObservation);
            };
        }

        private void SetupButtons(ObservationContainer observationContainer)
        {
            FindViewById<Button>(Resource.Id.okButton).Click += (sender, e) =>
            {
                observationContainer.SaveActivityState(SelectedObservation);
            };

            FindViewById<Button>(Resource.Id.saveButton).Click += (sender, e) =>
            {
                observationContainer.SaveActivityState(SelectedObservation);
            };
        }

        private void SetupDangerGridLayout()
        {
            GridLayout addEdits = FindViewById<GridLayout>(Resource.Id.goHide);

            isDangerSwitch.CheckedChange += (sender, args) =>
            {
                if (args.IsChecked)
                    addEdits.Visibility = ViewStates.Visible;
                else
                    addEdits.Visibility = ViewStates.Invisible;
            };
        }
    }
}